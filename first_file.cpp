#include <iostream>
using namespace std;
#include <opencv2/opencv.hpp>
using namespace cv;
#include <QFile>
#include <thread>
#include <shared_mutex>
#include <fstream>
std::shared_timed_mutex write_;

inline void do_in_parallel(Mat& img,double& time1,double& sum)
{
    static int i1{0};i1++;
    double t1=getTickCount();
    parallel_for_(Range(0,5000),[&](const Range& rangey)
    {
        for (int y = rangey.start; y < rangey.end; ++y)
        {
            parallel_for_(Range(0,5000),[&](const Range& range)
            {
                for (int var = range.start; var <range.end; ++var)
                {
                    img.at<float>(y,var)=10;
                }
            });
        }
    });
    time1=(getTickCount()-t1)/getTickFrequency();
    cout<<"\033[1;31m"<<i1<<" Time elapsed(multi_thread)="<<time1<<endl;
    sum+=time1;
}

inline void do_in_sequence(Mat& img , double& time2,double& sum)
{
    static int i1{0}; i1++;
    double t1=getTickCount();
    for (int row = 0; row < 5000; ++row)
    {
        for (int col = 0; col < 5000; ++col)
        {
            img.at<float>(row,col)=10;
        }
    }
    time2=(getTickCount()-t1)/getTickFrequency();
    cout<<"\033[1;32m"<<i1<<" Time elapsed(one_thread)="<<time2<<endl;
    sum+=time2;
}
int main()
{
    ofstream result_file;
    result_file.open("/home/nict/Desktop/result.csv");
    cout<<cv::getNumberOfCPUs()<<endl;
    Mat img(5000,5000,CV_32F);
    double time1,time2;
    for (int numT = 1; numT <30; ++numT)
    {
        setNumThreads(numT);
        double sum1{0},sum2{0};
        for (int iii = 0; iii < 10; ++iii)
        {
            do_in_parallel(img,time1,sum1);
            do_in_sequence(img,time2,sum2);
        }
        result_file<<numT<<","<<sum1<<","<<sum2<<endl;
    }

    result_file.close();
}